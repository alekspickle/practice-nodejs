'use strict';
module.exports = {
  up: (queryInterface, Sequelize) => {
    return queryInterface.createTable('Users', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: Sequelize.INTEGER,
          validate:{
            isNumeric: {
                msg: "Use only numbers."
            },
          }
      },
      name: {
        type: Sequelize.STRING,
          unique: true,
          validate:{
            notEmpty:true,
            isAlphanumeric: {msg: "Use only letters and numbers."},
          }
      },
      password: {
        allowNull: false,
        type: Sequelize.STRING,
        validate:{
            len:[8, 30],
        }
      },
      age: {
        type: Sequelize.INTEGER,
        validate:{
            max: 150,
        }
      },
      occupation: {
        type: Sequelize.STRING
      },
        token:{
          type:Sequelize.STRING
        },
      createdAt: {
        allowNull: false,
        type: Sequelize.DATE
      },
      updatedAt: {
        allowNull: false,
        type: Sequelize.DATE
      }
    });
  },
  down: (queryInterface, Sequelize) => {
    return queryInterface.dropTable('Users');
  }
};
