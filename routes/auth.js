const jwt = require("jsonwebtoken");
const express = require('express');
const router = express.Router();
const models = require('../models');
const config = require('../config/config');

router.post('/logIn', (req, res) => {
    const {name, password} = req.body;
    models.User
        .findOne({where:
                {name: req.body.name, password: req.body.password}})
        .then(user => {
            //User instance validation.
            if(!user){res.sendStatus(404)}else {
                //Check the password
                user.checkPassword(password, (err, isMatch) => {
                    if (err) {
                        console.log(err);
                    } else {
                        res.status(200).json({
                            token: jwt.sign({name}, config.secret, {
                                expiresIn: "24h"
                            })
                        })
                    }
                })
            }
        })
        .catch(err => res.sendStatus(500).json({error: "Invalid password."}))
});

router.post('/addUser', (req, res) => {
    const {name, password, age, occupation} = req.body;
   models.User
       .create({name, password, age, occupation})
       .then(res.sendStatus(200).json({
           token: jwt.sign({name}, config.secret, {
               expiresIn:"24h"
           })}
       ))
       .catch(err => {
           console.log(err);
           res.sendStatus(500).send("Incorrect username.")
       })
});


module.exports = router;
