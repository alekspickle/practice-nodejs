const models = require('../models');
const express = require('express');
const router = express.Router();
const jwt = require('jsonwebtoken');
const config = require('../config/config');


router.get('/create', (req, res) => {
   models.User
       .create()
       .then(users => res.json(users))
       .catch(err => res.sendStatus(404).send("There is no one here, try add users first, honey."))
});

router.get('/:id', (req, res) => {
    models.User
        .findOne({id: req.params.id})
        .then(user => res.json(user))
        .catch(err => res.status(404).send('No such user. Are you sure you at the right place?')|| console.log(err));
});


router.post('/:id/edit', (req, res) => {
    const user = models.User.getById(req.params.id);
    user.update({where: {name: req.body.name}})
        .then(user => res.json(user))
        .catch(err => res.status(404).send('Probably you should check an update method, dumbass.')|| console.log(err));
});


router.delete('/:id/delete', (req, res) => {
    jwt.verify(req.query.token, config.secret, (err, decoded) => {
        if(err){
            res.sendStatus(406).send("You have no right to do this! I\`m going to need my lawyer.")
        }else{
            const user =  models.User.findById(req.params.id);
            user.destroy({where:{id: req.body.id}})
                .then(res.send("User deleted."))
                .cath(err => console.log(err))
        }
    });

});

module.exports = router;
