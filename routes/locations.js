const models = require('../models');
const express = require('express');
const router = express.Router();

router.get('/:id', (req, res) => {
   models.Location
       .findOne({where: {id:req.params.id }})
       .then(location => res.json(location))
       .catch(err => res.sendStatus(404).send("Location not found. If it is not there it is not there."))
});


router.post('/newLocation', (req, res) => {
    const {userName, type, lat, lng} = req.body;
    models.Location
        .create({name: userName, type: type, lat: lat, lng: lng})
        .then(location => {
            res.json(location)
        })
        .catch(err => res.sendStatus(406).send("Something went wrong with the place...well at least you tried."))
});
router.post('/:id/edit', (req, res) => {
    const user = models.Location.findById(req.params.id);
    user.update({where: {id: req.params.id}})
        .then(location => res.json(location))
        .catch(err => res.sendStatus(406).send("Sorry, cannot edit the location."))
});

router.delete('/delete', (req, res) => {
    models.Location
        .destroy({where: {name: req.body.name}})
        .then(res.send("Location deleted!"))
        .catch(err => res.sendStatus(406).send("Something went wrong."))
});

module.exports = router;