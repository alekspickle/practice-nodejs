'use strict';
const bCrypt = require('bcrypt');

module.exports = (sequelize, DataTypes) => {
    const saltRounds = 10;

    const classMethods = {
        associate: models => {
            models.User.hasMany(models.Location);
        }
    };

    const instanceMethods = {
        checkPassword: (passwordToCheck, cb) => {
            bCrypt.compare(passwordToCheck, this.password, (err, isMatch) => {
                if (err) return cb(err);
                cb(null, isMatch);
            });
        }
    };

    const model = {
        name: {
            type: DataTypes.STRING,
            unique: {msg: "This User is already exists."},
            validate:{
                isAlphanumeric: {msg: "Use only letters and numbers."}
            }
        },
        password: {
            allowNull: false,
            type: DataTypes.STRING,
            validate:{
                len:[8, 30],
            }
        },
        age: {
            type: DataTypes.INTEGER,
            validate:{
                max: 150,
            }
        },
        occupation: {
            type: DataTypes.STRING
        },
    };

    const User = sequelize.define('User', model, {classMethods, instanceMethods});

    User.beforeCreate((user) => {
        return bCrypt.hash(user.password, saltRounds).then(hash => user.password = hash);
    });

    return User;
};
