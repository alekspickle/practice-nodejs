'use strict';
module.exports = (sequelize, DataTypes) => {
    const classMethods = {
        associate: models => {
            models.Location.belongsTo(models.User, {
                onDelete: "CASCADE",
                foreignKey: {
                    allowNull: false
                }
            });
        }
    };
    const model = {
        name: {
            type: DataTypes.STRING,
            unique: true,
            validate:{
                notEmpty: true,
                isAlphanumeric: true,
            }
        },
        type: {
            type: DataTypes.STRING,
            validate:{
                isAlphanumeric: true,
            }
        },
        lat: {
            type: DataTypes.INTEGER,
            validate:{
                allowNull: true,
                defaultValue: null,
                validate: { min: -90, max: 90 }
            }
        },
        lng: {
            type: DataTypes.INTEGER,
            validate:{
                allowNull: true,
                defaultValue: null,
                validate: { min: -180, max: 180 }
            }
        },
    };

    return sequelize.define('Location', model, {classMethods});
};
